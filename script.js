// Теоретичні питання
// Опишіть своїми словами що таке Document Object Model (DOM)

// В моём понимании DOM это страница html но глазами JavaScript

// Яка різниця між властивостями HTML-елементів innerHTML та innerText?

// Как я это вижу. Когда мы на занятии присваивали ссылку к тексту через innerText она заменяла текст, а вот через InnerHtml она добавляла гиперссылку к тексту.

// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// querySelector , querySelectorAll ,getElementById	id ,getElementsByName ,getElementsByTagName ,getElementsByClassName. Самые популнярные способы это - querySelector і querySelectorAll

// Завдання
// Код для завдань лежить в папці project.
//
//  Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let paragraphs = document.getElementsByTagName("p")
for (const paragraphsElement of paragraphs) {
    paragraphsElement.style.background = "red"
}
// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
let optionalList = document.getElementById('optionsList')
console.log(optionalList)
console.log(optionalList.parentNode)
for (let optionalListElement of optionalList.childNodes) {
    console.log(`${optionalListElement.nodeName} - ${optionalListElement.nodeType}`)
}

//     Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

document.getElementById("testParagraph").textContent = "This is a paragraph"

// Отримати елементи  li, вкладені в елементі із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item

let header = document.querySelectorAll(".main-header> .main-header-content >.d-flex > li")
for (let headerElement of header) {
    headerElement.classList.add("nav-item")
}
//Если полностью новый класс - то старый можно заменить с помощью .replace на новый. Касательно получение элементов li в main-header я решил таким способом. Потому что если без стрелочек, оно не находит в main-header > li

//     Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let sectionTitle = document.querySelectorAll(".section-title")
for (let sectionTitleElement of sectionTitle) {
   sectionTitleElement.classList.remove("section-title")
}
console.log(sectionTitle)